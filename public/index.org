#+Title: Salzinnes : son avenir nous appartient !
#+Author: Séance publique du collectif Salzinnes-Demain ASBL
#+Email: info@salzinnes-demain.org 
#+Date: 28 Novembre 2019

#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'cube'
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: moon
#+REVEAL_HLEVEL: 1
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Salzinnes demain - soirée publique.">
#+REVEAL_POSTAMBLE: <p> Created by Fred Vanwin. </p>

# #+STARTUP: overview
# #+STARTUP: hidestars
# #+STARTUP: logdone
# #+COLUMNS: %38ITEM(Details) %7TODO(To Do) %TAGS(Context) 
# #+OPTIONS: tags:t 
# #+OPTIONS: timestamp:t p:t
# #+OPTIONS: todo:t
# #+OPTIONS: TeX:t
# #+OPTIONS: LaTeX:t          
# #+OPTIONS: skip:t @:t ::t |:t ^:t f:t
#+OPTIONS: reveal_single_file:t

# # Comment for LaTeX
# #+OPTIONS: num:nil toc:nil

#+REVEAL_MARGIN: 0.1
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TRANS: fade
#+REVEAL_EXTRA_CSS: ./salzinnes_demain.css
# #+REVEAL_PLUGINS: (highlight markdown notes)
# #+PROPERTY: results output
# #+PROPERTY: exports both

# #+LaTeX_CLASS: koma-article
# # #+LATEX_HEADER: \usepackage[margin=0.5in]{geometry}

* Introduction
* Création de l'ASBL
** Histoire
[[./images/ligne_du_temps.png]]
** But social
#+BEGIN_QUOTE
L'association a pour but social de mobiliser et de promouvoir les forces vives
du faubourg de Salzinnes dans une démarche citoyenne participative,
collaborative, constructive et solidaire, et ce, en application des concepts de
«Quartier durable et de Transition».
#+END_QUOTE

* Charte 
- Soyons *acteurs* plutôt que moralisateurs
- Soyons *accessibles* au plus grand nombre
- Soyons *unis* : ensemble, nous allons plus loin
- Soyons *résilients* pour absorber les chocs
- Soyons *vigilants* et *proactifs* pour assurer un développement plus durable de notre quartier
- Soyons *pour* au lieu de se positionner contre
- Soyons « *local* » pour un impact global
- Soyons *créatifs* en imaginant des alternatives
- *Cultivons la bienveillance*
  
* Finances & Communication 
** Le point sur la situation financière
#+ATTR_REVEAL: :frag (appear)
Budget et comptes annuels seront présentés à l'AG début 2020 ! Bienvenue à tous les membres =;-)=

** Annonce : SAVE THE DATE ! rdv ici même
#+ATTR_HTML: :style font-size:200%
Organisation d'une marche Adeps le *dimanche 23 février* 2019, à l'initiative du comité de quartier Kegeljan

** Communication
#+ATTR_REVEAL: :frag (grow)
- Formulaire de contact pour ce soir (!) :: https://tinyurl.com/salzinnes-demain
#+ATTR_REVEAL: :frag (appear)
- Site web :: [[http://www.salzinnes-demain.org]] 
- Messagerie & newsletter :: [[mailto:info@salzinnes-demain.org]]
- Facebook :: "Collectif Salzinnes-Demain"
- Autres :: twitter, ...

* Les groupes de travail
** Carte générale des projets
[[./images/Salzinnes_WC.png]]

** Carte générale des projets
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :UNNUMBERED: t
    :END:

[[./images/Salzinnes_WC_carte.png]]

** Les grands projets : la MAP
[[./images/MAP.jpg]]
*** 
    :PROPERTIES:
    :reveal_background: ./images/MAP.jpg
    :reveal_background_size: 100%
    :END:

*** Les étapes de la MAP - avant 2018
    :PROPERTIES:
    :reveal_background: ./images/MAP.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:

- 2010-2017 :: La Province décide de regrouper tous ses services dans un nouveau bâtiment et élabore le projet de la MAP
- Mars 2018 :: Demande de permis introduite par la Province suivi de l'enquête publique (Avril-Mai 2018)
- Mai & Juillet 2018 :: Avis favorable au projet MAP remis par la Ville et le fonctionnaire délégué de la Région Wallonne 
- ==> :: Le Collectif s'oppose au projet et introduit un recours


*** Les étapes de la MAP - 2018
    :PROPERTIES:
    :reveal_background: ./images/MAP.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
- Août 2018 :: Province & Ville s'engagent à la création d'un passage sous voie pour relier la rue Woitrin à la rue Henri Blès  (entièrement financé par la Province)
- Novembre 2018 :: Rejet du recours introduit par le Collectif
- ==> :: Mise en place d'un comité d'accompagnement du chantier de la MAP (demande souhaitée par le Collectif)

*** Les étapes de la MAP - 2019 et ...
    :PROPERTIES:
    :reveal_background: ./images/MAP.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
- Janvier 2019 :: Installation du chantier par la société Jan de Nul
- Décembre 2020 :: Fin du chantier
- Janvier 2021 :: Installation des services provinciaux dans la MAP
- + Fin 2019 :: Vente des autres bâtiments de la Province (10 sont situés à Salzinnes), source d'interrogation pour le faubourg ...

** Groupe de travail "mobilité"
[[./images/mob3.jpg]]

*** Pouvons nous parler mobilité sans évoquer :
- la santé publique
- l'aménagement du territoire
- le partage de l'espace public
- la biodiversité
- la sécurité routière
- les nuisances sonores, visuelles, olfactives, ... ????

*** Avant 2018 : Synthèse des propositions de modifications/actualisation du Plan Communal de Mobilité (PCM)
- pas de nouveau pont
- report du trafic sur la N 90 G (Namur Expo) 
- système de transport intelligent (STI)
- mise à sens unique montant de l'avenue Cardinal Mercier au profit d'une bande bus + vélo descendante 
- priorisation des bus TEC par bandes d'approche aux feux (Val Saint-Georges ...) et giratoires place Falmagne
- évitement du transit dans les quartiers : étude de sens uniques, zone 30 …
- reprofiler le Pont de la Libération - fluidifier trafic - retour aux feux ?
- rabattement surplus du trafic Nord Ouest N 4 vers la Sambre 

#+REVEAL: split
NB : PCM "adopté dans sa philosophie générale" par le Collège en juin 2019. 

#+REVEAL: split
[[./images/mob1.jpg]]

*** Nos observations et réactions (courriers et rencontres)
- tableau synoptique analytique des différentes propositions : suggestions, remarques, ...
- cadastre des trottoirs
- statut et état des voiries
- cadastre du stationnement/parking domaine privé et domaine public
- proposition d'un P+R à l'entrée ouest (terrain Woitrin)
- augmentation desserte SNCB (Flawinne-Ronet-Jambes)
- amélioration rives de Sambre
- recours à la voie d'eau
- grand sens unique tout autour de Salzinnes Centre ?

*** 
    :PROPERTIES:
    :reveal_background: ./images/mob2.jpg
    :reveal_background_size: 100%
    :END:

*** En 2018 : Réponses des échevins
- Travaux et voiries :: A.R. + transmis aux services techniques pour organiser une rencontre : plan trottoirs - sécurité - équipements urbains- suivi de chantiers
- Environnement, Propreté :: A.R. + collaboration avec l'ISSEP et l' AWAC : déjà des mesures de qualité de l'air urbain - voir STI
- Urbanisme, Cohésion Sociale :: A.R. et vive attention  ..
- --> :: Pas de mise en place de Schéma d'Orientation Locale (SOL) 

*** Enjeux liés à la Maison Administrative Provinciale (MAP)
- espace public phagocyté - disparition espaces verts 
- Circulation et stationnement
- Nuisances visuelles, sonores
- Qualité de l'air
- enquête publique mai 2018 -recours
- rencontre Bourgmestre - comité d' accompagnement 
- Avenir des sites provinciaux mis en vente - appel d'air

*** En 2019, 3 axes : 
- qualité de l'air
- giratoire place du Huit mai
- zone 30 scolaire

*** Qualité de l'air 
- mesures Greenpeace rue Patenier 
- Rencontre échevines Mobilité et Transition Ecologique
- Actions envisagées : limiter le trafic lourd en reportant sur la N 90 G 

*** Giratoire place du Huit Mai :
[[./images/mob4.jpg]]

#+REVEAL: split
- Avril 2019 annonce par l'échevine Mobilité : fluidifier le trafic et améliorer qualité de l'air
- Contacts avec Directions d'Ecoles et Associations de Parents et association des commerçants  
- Courrier à la Ville avec liste de questions :
- Réponse : mesure provisoire, a réévaluer dans le cadre du PCM
- Début août chantier
- Rentrée scolaire : observations 

*** Zone 30 scolaire
- obligation réglementaire
- SPW et Ville en défaut
- courrier aux deux instances pour mise en conformité
- Réponse :
  - étonnement sur le ton du courrier
  - mise à l'étude 


** Les samedis de la transition
#+ATTR_REVEAL: :frag (appear)
   [[./images/transition.jpg]]

** La convivialité : place Godin et les alentours

*** 
    :PROPERTIES:
    :reveal_background: ./images/convivialite.jpg
    :reveal_background_size: 100%
    :END:

*** Les grandes étapes
    :PROPERTIES:
    :reveal_background: ./images/convivialite.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
- *2017*
  - Projet Convivialité selon 3 axes : mobilité, espace vert, espace de rencontres
  - Diagnostic marchant sur la Place Godin (consultations commerçants)
- *2018* 
  - Travail sur une proposition d'aménagement (consultations comité de quartier, SD, etc.)
  - Présentation du projet à la Ville
*** Les grandes étapes
    :PROPERTIES:
    :reveal_background: ./images/convivialite.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
- *2019*
  - Présentation du projet aux échevins nouvellement élus
  - Présentation du projet à l'Hôpital Ste Elisabeth pour une partie "Mobilité"
- *2020*  
  - Budget Participatif

*** NB : Consultation citoyenne en cours sur le marché de la place Godin
    :PROPERTIES:
    :reveal_background: ./images/convivialite.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
[[http://bit.ly/salzinnessondage]]

** Le stand de tir de Ronet
[[./images/STR.jpg]]
*** 
    :PROPERTIES:
    :reveal_background: ./images/STR.jpg
    :reveal_background_size: 120%
    :END:

*** Les étapes du STR
    :PROPERTIES:
    :reveal_background: ./images/STR.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:

- Le Cercle de Tir de Namur exploite plusieurs pas de tir sur le domaine militaire de Ronet.
- Le permis d'exploitation de ce stand de tir devait être renouvelé en  juin 2018.
- Le Collectif profite de ce renouvellement pour obtenir :
  - l'arrêt des tirs le dimanche matin
  - la mise en conformité des installations vis-à-vis des normes de bruit

*** Les étapes du STR
    :PROPERTIES:
    :reveal_background: ./images/STR.jpg
    :reveal_background_size: 100%
    :reveal_background_opacity: 0.20
    :END:
- *Fin août 2018* la Ville renouvelle le permis d'exploitation, assorti des conditions :
  - exploitation mercredi et samedi (8h30-19h), dimanche (10h-12h)
  - obligation de mise en conformité vis-à-vis des normes de bruit confirmée par des mesures prises par un bureau d'acoustique agréé.

*A ce jour, cette mise en conformité est en cours mais n'est pas encore achevée ...*

** Une piste nouvelle évoquée : la ceinture alimentaire namuroire
- assurer l'accès à une alimentation saine pour tou·te·s, : autonomie et souveraineté alimentaire
- préserver la biodiversité et la qualité des sols : résilience des territoires
- garantir des emplois porteurs de sens, rémunérateurs non délocalisables : relocalisation de l'économie

Pistes d'action : organisation de ciné-débats dans les quartiers sur ce thème

Contact : Anne Thibaut (Salzinnes-les-Hauts)

* Séance d'interactions

* Séance d'ateliers autour des grands axes
